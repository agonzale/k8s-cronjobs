FROM python:alpine

RUN apk add gcc curl openssl krb5-dev libffi-dev musl-dev libxml2-dev libxslt-dev libressl-dev krb5 && \
    pip install python-cern-sso-krb

COPY sync.py /usr/bin/

#
#COPY certs/* /usr/local/share/ca-certificates/
# Better: https://gitlab.cern.ch/vcs/rclone-docker/blob/master/Dockerfile
RUN curl -o /usr/local/share/ca-certificates/cern-root.crt https://cafiles.cern.ch/cafiles/certificates/CERN%20Root%20Certification%20Authority%202.crt && \
    openssl x509 -inform DER -in /usr/local/share/ca-certificates/cern-root.crt -out /usr/local/share/ca-certificates/cern-root.pem -outform PEM && \
    rm /usr/local/share/ca-certificates/cern-root.crt && \
    curl -o /usr/local/share/ca-certificates/cern-grid.pem https://cafiles.cern.ch/cafiles/certificates/CERN%20Grid%20Certification%20Authority.crt && \
    update-ca-certificates && \
    cat /usr/local/share/ca-certificates/* >>$(python -m certifi) && \
    apk del gcc curl openssl && \
    rm -rf /var/cache/apk/*

ENTRYPOINT ["/usr/bin/sync.py"]
