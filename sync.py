#!/usr/bin/env python3
#
###############################################################################
'''
    Using Kerberos to authenticate, launches syncs wioth JIRA
'''

import sys
import json

from subprocess import (
    run,
    DEVNULL
)

from os import environ
import cern_sso
import requests

###############################################################################
def get_cookie(url):
    '''
        Gets the CERN SSO cookie
    '''

    try:
        cookie = cern_sso.krb_sign_on(url)
    except requests.exceptions.HTTPError as http_error:
        if http_error.response.status_code == 401:
            #
            try:
                username = '%s@%s' % (KUSER, KREALM)
            except NameError as nerr:
                print(nerr)
                print_help()
                sys.exit(5)
            cmd = ['kinit', username]
            error = run(cmd, input=KPASSWORD.encode(), stdout=DEVNULL,
                        stderr=DEVNULL).returncode
            if error:
                print("Cannot authenticate to Kerberos using %s as username" % username,
                      file=sys.stderr)
                sys.exit(4)
        #
        try:
            cookie = cern_sso.krb_sign_on(url)
        except requests.exceptions.HTTPError as http_error2:
            print(http_error2, file=sys.stderr)
            sys.exit(6)
        #

    return cookie
#
def sync_groups():
    '''
        Starts a Sync of eGroups
    '''

    rsp = requests.get(SYNCALLGROUPS, cookies=get_cookie(LOGINURL))

    if rsp.status_code != 200:
        print(SYNCALLGROUPS, file=sys.stderr)
        print('%s: %s' % (rsp.status_code, rsp.text), file=sys.stderr)
        sys.exit(rsp.status_code)
#
def sync_users():
    '''
        Starts a Sync of Users
    '''

    rsp = requests.get(SYNCALLUSERS, cookies=get_cookie(LOGINURL))
    if rsp.status_code != 200:
        print('%s: %s' % (rsp.status_code, rsp.text))
        sys.exit(rsp.status_code)
#
def reindex():
    '''
        Starts a reindex of issues
    '''

    cookies = get_cookie(LOGINURL)

    rsp = requests.get('%s?type=BACKGROUND' % REINDEX, cookies=cookies)

    if rsp.status_code != 200:
        print('%s: %s' % (rsp.status_code, rsp.text))
        sys.exit(rsp.status_code)

    rsp2 = requests.get('%s/progress' % REINDEX, cookies=cookies)

    if rsp2.status_code != 200:
        json_t = json.loads(rsp2.text)
        print('%s: %s' % (rsp2.status_code, json_t))
        sys.exit(rsp2.status_code)
#
def print_help():
    '''
        Prints help
    '''
    print("Use: %s [groups|users|reindex] <HOST>\n" % sys.argv[0], file=sys.stderr)
    print("Environment variables:\n\tKUSER:     The user to get the Kerberos ticket from.")
    print("\tKREALM:    The Kerberos Realm, if empty, 'CERN.CH' is used.")
    print("\tKPASSWORD: 'KUSER's password.\n")

###############################################################################

try:
    ACTION = sys.argv[1]
    HOST = sys.argv[2]
except IndexError as ierr:
    print(ierr)
    print_help()
    sys.exit(1)

try:
    KREALM = environ['KREALM']
except KeyError:
    KREALM = 'CERN.CH'

try:
    KUSER = environ['KUSER']
    KPASSWORD = environ['KPASSWORD']
except KeyError:
    pass

LOGINURL = 'https://%s/jira/loginCern.jsp' % HOST
SYNCALLGROUPS = 'https://%s/jira/rest/egroups/2.0/mapper/syncAllGroups' % HOST
SYNCALLUSERS = 'https://%s/jira/rest/egroups/2.0/mapper/syncAllUsers' % HOST
REINDEX = 'https://%s/jira/rest/api/2/reindex' % HOST

if ACTION == 'groups':
    sync_groups()
elif ACTION == 'users':
    sync_users()
elif ACTION == 'reindex':
    reindex()
else:
    print("ERROR: Action '%s' not found" % ACTION)
    print_help()
    sys.exit(2)
